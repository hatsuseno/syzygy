package Syzygy::Object;

use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ObjectReference
];

=head1 NAME

Syzygy::Object - Syzygy object metaclass

=head1 DESCRIPTION

This class binds all parts of an object together in a singularly referencable
whole.

=cut

use BTTW::Tools;
use Syzygy::Object::Instance;
use Syzygy::Types qw[Name ObjectType Source Value UUID];
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

=head1 ATTRIBUTES

=head2 id

L<UUID|Syzygy::Types/UUID> identifier of the object.

If not provided to the constructor, a random C<UUID> will be generated.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has id => (
        is => 'rw',
        isa => UUID,
        default => sub { create_uuid_as_string(UUID_V4) }
    );
}

=head2 type

Reference to an object implementing L<Syzygy::Interface::ObjectType>.

This object will specificy how the object behaves.

=head3 Delegates

=over 4

=item L<type_name|Syzygy::Interface::ObjectType/name>

=back

=cut

# BEGIN, so role application has the interface
BEGIN {
    has type => (
        is => 'ro',
        isa => ObjectType,
        required => 1,
        handles => {
            type_name => 'name'
        }
    );
}

=head2 instance

Optional reference to a L<Syzygy::Object::Instance> object. This object will
contain the objects values.

This attribute is auto-vivified via L</set_value> if one does not yet exist.

=head3 Delegates

=over 4

=item L<has_value|Syzygy::Object::Instance/has_value>

=item L<clear_value|Syzygy::Object::Instance/clear_value>

=back

=cut

has instance => (
    is => 'rw',
    isa => 'Syzygy::Object::Instance',
    predicate => 'has_instance',
    handles => {
        has_value => 'has_value',
        clear_value => 'clear_value'
    }
);

=head2 source_name

This L<name|Syzygy::Types/Name> references a L<Syzygy::Interface::Source> from
which the object was produced (if any).

This attribute also implies that a given object is referenceable. If no source
is provided, the object is treated as a transient/volatile piece of data which
lives only in memory.

If a source_name is set, the L</id> field is assumed to be the actual
identifier of the object in a given source.

=head3 Delegates

=over 4

=item has_source_name

Returns true if the attribute is set.

=back

=cut

has source_name => (
    is => 'rw',
    isa => Name,
    predicate => 'has_source_name'
);

=head1 METHODS

=head2 set_value

Sets a value in the L</instance> (which is auto-vivified if missing).

The value is first
L<processed|Syzygy::Interface::ObjectType/process_attribute_values> by the
L</type>, and only set if everything checks out.

    # Set the attribute using a plain perl value. The value type of the
    # attribute validates this in raw data context.
    $object->set_value('attribute_name', 'some value');

    # Explicitly set a typed value. If the type of the value does not
    # directly match, a coercion will be attempted via the target value type.
    $object->set_value('attribute_name', szg_value('text', 'abc'));

=cut

sub set_value {
    my $self = shift;
    my $attribute_name = shift;
    my $value = shift;

    Name->assert_valid($attribute_name);

    unless (Value->check($value)) {
        # Slightly nasty API, Type should provide single-attribute processors
        $value = $self->type->process_attribute_values({
            $attribute_name => $value
        })->{ $attribute_name };
    }

    return $self->_set_value($attribute_name, $value);
}

=head2 get_value

Returns a L<Syzygy::Object::Value> instance if one can be resolved for the
provided attribute name.

    my $value = $object->get_value('attribute_name');

=cut

sub get_value {
    my $self = shift;
    my $attribute_name = shift;

    Name->assert_valid($attribute_name);

    return $self->_get_value($attribute_name);
}

=head2 as_graph_node

Deflates the object's data to 'flat' datastructure for serialization and
exchange between systems.

This method returns a hashref that can be directly serialized as JSON, but
need not be.

    my $object = ...;

    my $data = $object->as_graph_hash;
    # {
    #   type => '$object->type_name',
    #   reference => '$object->id',
    #   instance => { ... }
    # }

Depending on the state of an object, the instance and/or reference may be
undefined. If L</instance> is undefined, the object will act as an object_ref
value. If the L</source_name> field is undefined, no reference will be
returned (even though one is auto-generated).

=cut

sub as_graph_hash {
    my ($self, @args) = @_;

    my $instance_hash;
    my $reference_id;

    if ($self->has_instance) {
        $instance_hash = $self->instance->as_graph_hash(@args);
    }

    if ($self->has_source_name) {
        $reference_id = $self->id;
    }

    return {
        type => $self->type_name,
        instance => $instance_hash,
        reference => $reference_id
    };
}

=head1 PRIVATE METHODS

=head2 _get_value

Low-level attribute getter.

=cut

sub _get_value {
    my $self = shift;
    my $attribute_name = shift;

    unless ($self->has_instance) {
        throw('syzygy/object/instance_not_set', sprintf(
            'Attempted dereference of attribute "%s" in empty object',
            $attribute_name
        ));
    }

    $self->_assert_type_attribute($attribute_name);

    return $self->instance->get_value($attribute_name);
}

=head2 _set_value

Low-level attribute value setter.

    $object->_set_value('my_attribute_name', szg_value('text', 'abc'));

Using this method, one can set a value instance with any type on the instance.
B<Don't do that>.

=cut

sub _set_value {
    my $self = shift;
    my $attribute_name = shift;
    my $value = shift;

    unless ($self->has_instance) {
        $self->instance(Syzygy::Object::Instance->new);
    }

    $self->instance->set_value($attribute_name, $value);

    return;
}

=head2 _assert_type_attribute

Asserts that the L</type> of the object instance has the specified attribute.

Used by L</get_value>, L</set_value>, and more methods to validate input.

=cut

sub _assert_type_attribute {
    my $self = shift;
    my $attribute_name = shift;

    unless ($self->type->has_attribute($attribute_name)) {
        throw('syzygy/object/type/attribute_does_not_exist', sprintf(
            'Attempted dereference of attribute "%s" in "%s"-type object, attribute not specified by type.',
            $attribute_name,
            $self->type_name
        ));
    }
}

=head2 _ref

Implements L<Syzygy::Interface::ObjectReference/_ref>.

=cut

sub _ref {
    my $self = shift;

    return Syzygy::Object::Reference->new(
        id => $self->id,
        type => $self->type_name
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
