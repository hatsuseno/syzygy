package Syzygy::Query::Expression;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools;

requires qw[
    stringify
    as_complex_value
];

sig stringify => '=> Str';

sig as_complex_value => '=> HashRef';

=head1 NAME

Syzygy::Query::Expression - Query expression interface

=head1 DESCRIPTION

This role identifies object query expressions.

=head1 REQUIRED METHODS

=head2 stringify

This role requires an implementation of a method that stringifies, in a
human-readable format, the expression it's state represents.

=head2 as_complex_value

Transforms the expression object to a hashref representation that can be
serialized.

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
