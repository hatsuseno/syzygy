package Syzygy::Query::Sort;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

=head1 NAME

Syzygy::Query::Sort - Sort declarations for object queries

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

A L<Syzygy::Query::Expression> that should be used for sorting
the results of the query, after the condition has been applied to the
resultset.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head2 reverse

Boolean attribute that indicates if the resultset should be sorted in reverse
order.

=cut

has reverse => (
    is => 'rw',
    isa => 'Bool',
    traits => [qw[Bool]],
    default => 0,
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

=cut

sub stringify {
    my $self = shift;

    my $ret = $self->expression->stringify;

    if ($self->reverse) {
        $ret = sprintf('%s, reverse order', $ret);
    }

    return $ret;
}

=head2 as_complex_value

Serializes the sort clause as a standard hashref.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        sort => {
            direction => $self->reverse ? 'reverse' : 'normal',
            expression => $self->expression->as_complex_value
        }
    };
}

=head1 FUNCTIONS

=head2 from_complex_value

Constructor for hashref representations of the sort clause of a query.

    my $sort = Syzygy::Query::Sort->from_complex_value({
        direction => 'reverse',
        expression => { ... }
    });

=cut

sub from_complex_value {
    my $class = shift;
    my $hash = shift;

    return $class->new(
        reverse => $hash->{ direction } eq 'reverse' ? 1 : 0,
        expression => Syzygy::Query::Expression->from_complex_value(
            $hash->{ expression }
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
