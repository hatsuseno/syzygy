package Syzygy::Query::Expression::Comparison;

use Moose;

use BTTW::Tools;

use Moose::Util::TypeConstraints qw[
    enum
    find_type_constraint
    role_type
];

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::Equal - Abstracts quality testing
conditonal

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 mode

Mode of comparison. One of C<equal>, C<not_equal>, C<less_than>,
C<greater_than>, C<equal_or_less_than>, or C<equal_or_greater_than>.

=cut

has mode => (
    is => 'rw',
    isa => enum([qw[
        equal
        not_equal
        less_than
        greater_than
        equal_or_less_than
        equal_or_greater_than
    ]]),
    default => 'equal',
    required => 1
);

=head2 expressions

Collection of expressions that are tested for equality.

=cut

has expressions => (
    is => 'rw',
    isa => find_type_constraint('ArrayRef')->parameterize(
        role_type('Syzygy::Query::Expression')
    ),
    required => 1,
    traits => [qw[Array]],
    handles => {
        all_expressions => 'elements'
    }
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    # "my_field equal to 'abc'"
    qb_eq('my_field', 'abc');

=cut

sub stringify {
    my $self = shift;

    return join sprintf(' %s ', $self->mode), map {
        $_->stringify
    } $self->all_expressions;
}

=head2 as_complex_value

Serializes the comparison expression as a standard hashref.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        comparison => {
            mode => $self->mode,
            expressions => [
                map { $_->as_complex_value } $self->all_expressions
            ]
        }
    };
}

=head1 FUNCTIONS

=cut

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
