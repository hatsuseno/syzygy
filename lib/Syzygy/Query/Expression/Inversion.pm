package Syzygy::Query::Expression::Inversion;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::Inversion - Abstracts conditional
inversions (logical NOT)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

The expression to be inverted.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    # "not my_field equal to 'abc'"
    qb_not(qb_eq('my_field', 'abc'))->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf('not %s', $self->expression->stringify);
}

=head2 as_complex_value

Implements L<Syzygy::Query::Expression/as_complex_value>.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        inversion => {
            expression => $self->expression->as_complex_value
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
