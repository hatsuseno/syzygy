package Syzygy::Query::Expression::ContainsString;

use Moose;

use Moose::Util::TypeConstraints qw[role_type enum];
use BTTW::Tools;

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::ContainsString - Simple string pattern
matching expression.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 string

The string to match to.

=cut

has string => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head2 match

The string to match

=cut

has match => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head2 mode

The mode of the string search, either C<prefix>, C<infix>, or C<postfix>.

If not set, the match must be exact.

=cut

has mode => (
    is => 'rw',
    isa => enum([qw[prefix infix postfix]]),
    predicate => 'has_mode',
    clearer => 'clear_mode'
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    "infix match field:my_field to text('abc')"

=cut

sub stringify {
    my $self = shift;

    my $mode = $self->has_mode
        ? sprintf('%s match', $self->mode)
        : 'match';

    return sprintf(
        '%s %s to %s',
        $mode,
        $self->match->stringify,
        $self->string->stringify
    );
}

=head2 as_complex_value

Implements L<Syzygy::Query::Expression/as_complex_value>.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        contains_string => {
            mode => $self->mode,
            string => $self->string->as_complex_value,
            match => $self->match->as_complex_value
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
