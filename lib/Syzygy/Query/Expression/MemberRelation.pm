package Syzygy::Query::Expression::MemberRelation;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::MemberRelation - Abstracts the
'is member of' logic of IN conditionals.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

The expression for which membership in L</set> is tested.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head2 set

The set for which membership of L</expression> is tested.

=cut

has set => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    # "my_field is member of [ text('abc'), text('def'), ... ]
    qb_in('my_field', [qw[abc, def]])->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s is member of %s',
        $self->expression->stringify,
        $self->set->stringify
    );
}

=head2 as_complex_value

Implements L<Syzygy::Query::Expression/as_complex_value>.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        member_relation => {
            expression => $self->expression->as_complex_value,
            set => $self->set->as_complex_value
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
