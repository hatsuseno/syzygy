package Syzygy::Query::Expression::Literal;

use Moose;

use BTTW::Tools;
use Moose::Util::TypeConstraints qw[enum];
use Syzygy::Types qw[Name];

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::Literal - Abstracts literal expression
values (strings, timestamps, etc)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 type

Type of the value.

=cut

has type => (
    is => 'rw',
    isa => Name,
    required => 1
);

=head2 value

The value of the literal.

=cut

has value => (
    is => 'rw',
    isa => 'Defined',
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    # "text(abc)"
    qb_lit('text', 'abc')->stringify

    # "object(person(unsynched))"
    qb_lit('object', Syzygy::Object::Types::Person->new)->stringify

    # "object(case(...fb3599))"
    qb_lit('object', $my_case_instance)->stringify

=cut

sub stringify {
    my $self = shift;
    my $str = $self->value;

    if ($self->type eq 'object') {
        $str = $str->_as_string;
    }

    return sprintf('%s(%s)', $self->type, $str);
}

=head2 as_complex_value

Implements L<Syzygy::Query::Expression/as_complex_value>.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        literal => {
            value_type_name => $self->type,
            value => $self->value
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
