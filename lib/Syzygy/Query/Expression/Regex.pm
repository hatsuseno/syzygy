package Syzygy::Query::Expression::Regex;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::Regex - Abstracts conditional regex
expressions

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 string

The 'string' to match L</pattern> against.

=cut

has string => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    required => 1
);

=head2 pattern

The pattern to match against L</string>.

=cut

has pattern => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    qb_re('my_field', '^a.cde[fg]+$')->stringify
    # "myfield matches /^a.cde[fg]+$/"

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s matches /%s/',
        $self->string->stringify,
        $self->pattern
    );
}

=head2 as_complex_value

Implements L<Syzygy::Query::Expression/as_complex_value>.

=cut

sub as_complex_value {
    my $self = shift;

    return {
        regex => {
            string => $self->string->as_complex_value,
            pattern => $self->pattern
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
