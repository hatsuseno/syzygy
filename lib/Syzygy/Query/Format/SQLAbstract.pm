package Syzygy::Query::Format::SQLAbstract;

use Moose;
use namespace::autoclean;

=head1 NAME

Syzygy::Query::Format::SQLAbstract - Format a L<Syzygy::Query> as input data
for L<SQL::Abstract>.

=head1 SYNOPSIS

    use Syzygy::Syntax;
    use Syzygy::Query::Format::SQLAbstract;

    my $query = qb(...);
    my $formatter = Syzygy::Query::Format::SQLAbstract->new;

    my $resultset = ...;

    my $new_resultset = $resultset->search($formatter->format($query));

=cut

use BTTW::Tools;

=head1 METHODS

=head2 query_to_sql_abstract

Translates a L<Syzygy::Query> object to
L<SQL::Abstract>-compatible parameters.

    my ($search, $opts) = $rs->query_to_sql_abstract(qb('my_object', { ... }));

=cut

sig query_to_sql_abstract => 'Syzygy::Query';

sub query_to_sql_abstract {
    my $self = shift;
    my $query = shift;

    my $search;
    my $opts;

    if ($query->has_cond) {
        $search = _unfold_query_expr($query->cond);
    }

    if ($query->has_sort) {
        my $order = $query->sort->reverse ? '-desc' : '-asc';

        $opts = {
            order_by => {
                $order => _unfold_query_expr($query->sort->expression)
            }
        };
    }

    return ($search, $opts);
}

sub _unfold_query_expr {
    my $expr = shift;
    my $field_prefix = shift || '';

    if ($expr->isa('Syzygy::Query::Expression::Conjunction')) {
        return { -and => [ map { _unfold_query_expr($_) } $expr->all_expressions ] };
    }

    if ($expr->isa('Syzygy::Query::Expression::Disjunction')) {
        return { -or => [ map { _unfold_query_expr($_) } $expr->all_expressions ] };
    }

    if ($expr->isa('Syzygy::Query::Expression::Comparison')) {
        my %mode_map = (
            equal                 => '=',
            not_equal             => '!=',
            less_than             => '<',
            greater_than          => '>',
            equal_or_less_than    => '<=',
            equal_or_greater_than => '>='
        );

        my ($first, $second, @rest) = $expr->all_expressions;

        unless (defined $first && defined $second) {
            throw('object/query/dbix_shim/empty_comparison', sprintf(
                '"%s": not enough comparison operands',
                $expr->stringify
            ));
        }

        if (scalar @rest) {
            throw('object/query/dbix_shim/unsupported_comparison', sprintf(
                '"%s": too many comparison operands',
                $expr->stringify
            ));
        }

        # Minor optimization of SQL::Abstract generator, use field => { ... }
        # style when we can, otherwise resort to bare SQL.
        if ($first->isa('Syzygy::Query::Expression::Field')) {
            my $fieldname = sprintf('%s%s', $field_prefix, $first->name);

            return {
                $fieldname => {
                    $mode_map{ $expr->mode } => _unfold_query_expr($second)
                }
            };
        }

        # Default case, merge all SQL blobs literally
        return _merge_sql_statements(' ', _merge_sql_exprs(
            _unfold_query_expr($first),
            $mode_map{ $expr->mode },
            _unfold_query_expr($second)
        ));
    }

    if ($expr->isa('Syzygy::Query::Expression::Literal')) {
        if ($expr->type eq 'datetime') {
            return \[ "?::timestamp without time zone", $expr->value->iso8601 ];
        }

        if ($expr->type eq 'text' || $expr->type eq 'string') {
            return \[ "?::text", $expr->value ];
        }

        if ($expr->type eq 'object_ref') {
            return \[ "?", $expr->value->id ];
        }

        throw('object/query/dbix_shim/unsupported_literal', sprintf(
            '"%s": literal value not suported',
            $expr->stringify
        ));
    }

    if ($expr->isa('Syzygy::Query::Expression::Set')) {
        return [ map { _unfold_query_expr($_) } $expr->all_expressions ];
    }

    if ($expr->isa('Syzygy::Query::Expression::Field')) {
        return \[ sprintf('%s%s', $field_prefix, $expr->name) ];
    }

    if ($expr->isa('Syzygy::Query::Expression::MemberRelation')) {
        if ($expr->expression->isa('Syzygy::Query::Expression::Field')) {
            my $fieldname = sprintf('%s%s', $field_prefix, $expr->expression->name);

            return {
                $fieldname => {
                    -in => _unfold_query_expr($expr->set)
                }
            };
        }

        return _merge_sql_statements(' ', _merge_sql_exprs(
            _unfold_query_expr($expr->expression),
            'IN',
            _merge_sql_statements(', ', _merge_sql_exprs(
                map { _unfold_query_expr($_) } $expr->set->all_expressions
            ))
        ));
    }

    throw('object/query/dbix_shim/unsupported_query', sprintf(
        '"%s": query not supported for automatic SQL::Abstract translation',
        $expr->stringify
    ));
}

sub _unfold_query_sort {
    # TODO implement sort transformation
    return undef;
}

sub _merge_sql_statements {
    my ($separator, $statements, @bind) = @_;

    return \[ sprintf('( %s )', join($separator, @{ $statements })), @bind ];
}

sub _merge_sql_exprs {
    my @statements;
    my @bind;

    for my $literal (@_) {
        if (ref $literal eq 'REF') {
            my ($part_stmt, @part_bind) = @{ ${ $literal } };

            push @statements, $part_stmt;
            push @bind, @part_bind;

            next;
        }

        unless (ref $literal) {
            push @statements, $literal;

            next;
        }

        throw('object/query/dbix_shim/unsupported_literal_sql', sprintf(
            'Cannot merge literal SQL "%s"',
            $literal // '<undefined>'
        ));
    }

    return \@statements, @bind;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
