package Syzygy::Query;

use Moose;
use namespace::autoclean;

use Moose::Exporter;
use Moose::Util::TypeConstraints qw[role_type];

use BTTW::Tools;

use Syzygy::Query::Expression::Conjunction;
use Syzygy::Query::Expression::Disjunction;
use Syzygy::Query::Expression::Inversion;
use Syzygy::Query::Expression::Comparison;
use Syzygy::Query::Expression::Literal;
use Syzygy::Query::Expression::ContainsString;
use Syzygy::Query::Expression::MemberRelation;
use Syzygy::Query::Expression::Regex;
use Syzygy::Query::Expression::Field;
use Syzygy::Query::Expression::Set;
use Syzygy::Query::Sort;
use Syzygy::Types qw[Name];

Moose::Exporter->setup_import_methods(
    as_is => [qw[
        qb
        qb_and qb_or qb_not qb_in qb_cmp
        qb_eq qb_ne qb_lt qb_gt qb_elt qb_egt
        qb_like qb_re
        qb_field qb_lit qb_set
        qb_sort
    ]]
);

=head1 NAME

Syzygy::Query - High-level object query abstraction and API

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 type

Refers to the C<type> of the L<Syzygy::Object> which instances of a
query should search for.

=cut

has type => (
    is => 'rw',
    isa => Name,
    required => 1
);

=head2 cond

Holds a reference to the root conditional expression with which the data
source should be queried.

Values must be objects which implement
L<Syzygy::Query::Expression>.

=cut

has cond => (
    is => 'rw',
    isa => role_type('Syzygy::Query::Expression'),
    predicate => 'has_cond',
    clearer => 'clear_cond'
);

=head2 sort

Holds a reference to a L<Syzygy::Query::Sort> object which should
be used for ordering of the results.

=cut

has sort => (
    is => 'rw',
    isa => 'Syzygy::Query::Sort',
    predicate => 'has_sort',
    clearer => 'clear_sort'
);

=head1 METHODS

=head2 stringify

Generates a human-readable string representing the current query.

=cut

sub stringify {
    my $self = shift;

    my @parts = (sprintf('search for "%s" objects', $self->type));

    if ($self->has_cond) {
        push @parts, sprintf('where %s', $self->cond->stringify);
    }

    if ($self->has_sort) {
        push @parts, sprintf('sorted by %s', $self->sort->stringify);
    }

    return join ', ', @parts;
}

=head2 as_complex_value

Transforms the query instance into a standard hashref form for use in API
exchanges.

=cut

sub as_complex_value {
    my $self = shift;

    my ($complex_cond, $complex_sort);

    if ($self->has_cond) {
        $complex_cond = $self->cond->as_complex_value;
    }

    if ($self->has_sort) {
        $complex_sort = $self->sort->as_complex_value;
    }

    return {
        type_name => $self->type,
        cond => $complex_cond,
        sort => $complex_sort
    };
}

=head1 FUNCTIONS

This collection of functions provide convenience calls to instantiators of
expression objects.

=head2 qb

Instantiates a new query.

    my $query = qb('case');

Additionally, a C<HashRef> can be used to directly set the query's attributes.

    my $query = qb('case', {
        cond => qb_eq('case.number', 1234)
    });

=cut

sub qb {
    my ($type, $args) = (@_, {});

    return __PACKAGE__->new({ type => $type, %{ $args } });
}

=head2 from_complex_value

Transforms a standard query hashref into a query instance.

=cut

sub from_complex_value {
    my $query_hashref = shift;

    my ($cond, $sort);

    if (defined $query_hashref->{ cond }) {
        Syzygy::Query::Expression->from_complex_value(
            $query_hashref->{ cond }
        );
    }

    if (defined $query_hashref->{ sort }) {
        Syzygy::Query::Sort->from_complex_value(
            $query_hashref->{ sort }
        );
    }

    my %args = ( type => $query_hashref->{ type_name } );

    $args{ cond } = $cond if defined $cond;
    $args{ sort } = $sort if defined $sort;

    return __PACKAGE__->new(\%args);
}

=head2 qb_and

    # 1 AND 2 == 3 AND abc ILIKE '%a%'
    qb_and(1, qb_eq(2, 3), qb_like('abc', 'a'))

=cut

sub qb_and {
    my @expressions = @_;

    return Syzygy::Query::Expression::Conjunction->new(
        expressions => \@expressions
    );
}

=head2 qb_or

    # 1 OR 'Kings' = 'Queens' OR my_field ~ '^my.*value$'
    qb_or(1, qb_eq('Kings', 'Queens'), qb_re('my_field', '^my.*value$'))

=cut

sub qb_or {
    my @expressions = @_;

    return Syzygy::Query::Expression::Disjunction->new(
        expressions => \@expressions
    );
}

=head2 qb_in

    # 'abc' IN (1, 2, 3, 4, 'a', 'b', 'c', 'abc')
    qb_in('abc', [qw[1 2 3 a b c abc]])

=cut

sub qb_in {
    my $expression = _qb_maybe_expr(shift);
    my $set = _qb_maybe_expr(shift);

    return Syzygy::Query::Expression::MemberRelation->new(
        expression => $expression,
        set => $set
    );
}

=head2 qb_not

    # field != 'def'
    qb_not(qb_eq('field', 'def'))

=cut

sub qb_not {
    return Syzygy::Query::Expression::Inversion->new(
        expression => shift
    );
}

=head2 qb_cmp

    # date_of_registration > '2017-10-10'
    qb_cmp('greater_than', qb_field('date_of_registration'), qb_lit('timestamp', DateTime->new(...)));

=cut

sub qb_cmp {
    return Syzygy::Query::Expression::Comparison->new(
        mode => shift,
        expressions => \@_
    );
}

=head2 qb_eq

    # requestor = <subject_uuid>
    qb_eq('requestor', $my_subject)

=cut

sub qb_eq {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('equal', $field, $value);
}

=head2 qb_ne

    # NOT(a = 'b')
    qb_ne('a', 'b')

=cut

sub qb_ne {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('not_equal', $field, $value);
}

=head2 qb_lt

    # my_field < 123
    qb_lt('my_field', 123)

=cut

sub qb_lt {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('less_than', $field, $value);
}

=head2 qb_gt

    # my_field > 123
    qb_gt('my_field', 123)

=cut

sub qb_gt {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('greater_than', $field, $value);
}

=head2 qb_elt

    # my_field <= 123
    qb_elt('my_field', 123)

=cut

sub qb_elt {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('equal_or_less_than', $field, $value);
}

=head2 qb_egt

    # my_field >= 123
    qb_egt('my_field', 123)

=cut

sub qb_egt {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return qb_cmp('equal_or_greater_than', $field, $value);
}

=head2 qb_like

    # my_field ILIKE '%text%'
    qb_like('my_field', 'text')

    # my_field ILIKE 'text%'
    qb_like('my_field', 'text', 'prefix')

=cut

sub qb_like {
    return Syzygy::Query::Expression::ContainsString->new(
        string => _qb_maybe_expr(shift),
        match => _qb_maybe_lit(shift),
        mode => (shift || 'infix')
    );
}

=head2 qb_re

    # my_field ~ '^a.[bc]+$'
    qb_re('my_field', '^a.[bc]+$')

=cut

sub qb_re {
    return Syzygy::Query::Expression::Regex->new(
        string => _qb_maybe_expr(shift),
        pattern => shift,
    );
}

=head2 qb_lit

    # 'abc'
    qb_lit('text', 'abc')

    # '2017-05-11T11:01:00'
    qb_lit('timestamp', DateTime->now)

    # 'e90cd356-ebfe-4b45-b9a8-7bc8aad8f4c3'
    qb_lit('object', Syzygy::Object->new)

=cut

sub qb_lit {
    return Syzygy::Query::Expression::Literal->new(
        type => shift,
        value => shift
    );
}

=head2 qb_set

    # ('abc', '518b9270-0be1-433f-88fb-e3cbb0e2986b')
    qb_set(qb_lit('text', 'abc'), qb_lit('object', Syzygy::Object->new))

=cut

sub qb_set {
    return Syzygy::Query::Expression::Set->new(
        expressions => \@_
    );
}

=head2 qb_field

    # my_field
    qb_field('my_field')

=cut

sub qb_field {
    return Syzygy::Query::Expression::Field->new(
        name => shift
    );
}

=head2 qb_sort

    # ORDER BY my_field DESC
    qb_sort('my_field', 'desc')

=cut

sub qb_sort {
    my $expr = _qb_maybe_expr(shift);
    my %reverse_map = ( asc => 0, desc => 1 );
    my $reverse = $reverse_map{ shift() || 'asc' };

    return Syzygy::Query::Sort->new(
        expression => $expr,
        reverse => $reverse
    );
}

# Privates and helpers here.

sub _qb_maybe_expr {
    my $expr = shift;
    my $ref = ref $expr;

    # Plain strings are interpreted as field references, so it's easy
    # to build conditions (qb_eq('my.field', 'my value'))
    return qb_field($expr) unless $ref;

    # Arrays become impliciet sets (see qb_in)
    return qb_set(map { _qb_maybe_lit($_) } @{ $expr }) if $ref eq 'ARRAY';

    # Maps become conjuncted equality conditionals, so querying with
    # { a => 'my_val', b => 'other val' } can be done concisely
    return qb_and(map { qb_eq($_, $expr->{ $_ }) } keys %{ $expr })
        if $ref eq 'HASH';

    return $expr if blessed($expr) && $expr->does('Syzygy::Query::Expression');

    throw('object/query_builder/expression_expected', sprintf(
        'Unexpected expression "%s", expected literal or object',
        $expr
    ));
}

sub _qb_maybe_lit {
    my $value = shift;

    my $ref = ref $value;

    # base case, we're a scalar, assume string context
    return qb_lit('string', $value) unless $ref;

    # plain references
    unless (blessed($value)) {
        if ($ref eq 'ARRAY') {
            return qb_set(map { _qb_maybe_lit($_) } @{ $value });
        }

        throw('object/query_builder/unsupported_literal', sprintf(
            'Unexpected %s-reference found in query',
            $ref
        ));
    }

    if ($value->isa('DateTime')) {
        return qb_lit('datetime', $value);
    }

    if ($value->can('does')->($value, 'Syzygy::Query::Expression')) {
        return $value;
    }

    if ($value->can('does')->($value, 'Syzygy::Object::Reference')) {
        return qb_lit('object_ref', $value->_ref);
    }

    throw('object/query_builder/unsupported_literal', sprintf(
        'Unexpected query part "%s" (ref "%s")',
        $value,
        $ref
    ));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
