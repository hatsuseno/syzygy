package Syzygy::Types;

use MooseX::Types -declare => [qw[
    AccessMode
    AttributeNamespace
    Name
    ObjectAttribute
    ObjectReference
    ObjectType
    Source
    Store
    UUID
    Value
    ValueType
]];

=head1 NAME

Syzygy::Types - L<MooseX::Types> library for working with the perl side of
L<Syzygy::Object>.

=cut

use List::Util qw[any];
use Moose::Util::TypeConstraints qw[enum];
use MooseX::Types::Moose qw[Str];
use UUID::Tiny qw[is_uuid_string];

use Data::Dumper;

=head1 TYPES

=head2 AccessMode

Specifies the valid values for the 'mode of access' concept. This specifically
applies to L</Source>, where a store may be set to the L<read-write> or
L<read-only> mode.

=cut

subtype AccessMode, as enum([qw[read-write read-only]]);

=head2 AttributeNamespace

Validates a string as an attribute's namespace.

Current values are C<sys> and C<usr>, which differentiate between dynamic
attributes (as defined by the user), or static attributes (as implied by
attributes declared in code).

=cut

subtype AttributeNamespace, as enum([qw[sys usr]]);

=head2 Name

Validates a 'name' for Syzygy entities.

Valid names are non-empty, lower-case, ascii-only, slug-like strings.

Examples C<case>, C<casetype_result>, or C<foo123_abc1233>.

=cut

subtype Name, as Str, where {
    my $match_one = qr/[a-z](?:[a-z0-9_]*[a-z0-9])?/;
    return length $_ > 0 && $_ =~ m/^$match_one(?:\/$match_one)*$/;
};

=head2 ObjectAttribute

Validates instances of L<Syzygy::Object::Attribute>.

=cut

class_type ObjectAttribute, { class => 'Syzygy::Object::Attribute' };

=head2 ObjectReference

Validates implementations of L<Syzygy::Interface::ObjectReference>.

=cut

role_type ObjectReference, { role => 'Syzygy::Interface::ObjectReference' };

=head2 ObjectType

Validates implementations of L<Syzygy::Interface::ObjectType>.

=cut

role_type ObjectType, { role => 'Syzygy::Interface::ObjectType' };

=head2 Source

Validates implementations of L<Syzygy::Interface::Source>.;

=cut

role_type Source, { role => 'Syzygy::Interface::Source' };

=head2 Store

Validates implementations of L<Syzygy::Interface::Store>.

=cut

role_type Store, { role => 'Syzygy::Interface::Store' };

=head2 UUID

Validates a C<UUID> value.

Implemented using L<UUID::Tiny/is_uuid_string>.

=cut

subtype UUID, as Str, where {
    is_uuid_string($_)
};

=head2 Value

Validates L<Syzygy::Object::Value> instances.

=cut

class_type Value, { class => 'Syzygy::Object::Value' };

=head2 ValueType

Validates implementations of L<Syzygy::Interface::ValueType>.

=cut

role_type ValueType, { role => 'Syzygy::Interface::ValueType' };

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
