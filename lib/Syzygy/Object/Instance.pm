package Syzygy::Object::Instance;

use Moose;
use namespace::autoclean;

=head1 NAME

Syzygy::Object::Instance - Base class for object instances

=head1 DESCRIPTION

This class is a glorified data holder, and only contains a map of slot
names to L<Syzygy::Object::Value> instances.

=cut

use BTTW::Tools;
use MooseX::Types::Moose qw[HashRef];
use Syzygy::Types qw[Value];

=head1 ATTRIBUTES

=head2 values

Map of slot names to L<Syzygy::Object::Value> instances.

=head3 Delegates

=over 4

=item has_value

Predicate for the existance of a value within an instance

=item get_value

Getter for the value.

=item set_value

Setter for the value.

=item clear_value

Clearer for the value (deletes it from the map)

=item all_values

Returns a list of all set values.

=item slot_names

Returns a list of slot names for which values are set.

=back

=cut

has values => (
    is => 'rw',
    isa => HashRef->parameterize(Value),
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_value => 'exists',
        get_value => 'get',
        set_value => 'set',
        clear_value => 'delete',
        all_values => 'values',
        slot_names => 'keys'
    }
);

=head1 METHODS

=head2 as_graph_hash

Implements deflating logic for serialization of objects.

This method will return a hashref of slot->value pairs for the instance.

=cut

sub as_graph_hash {
    my $self = shift;

    return {
        map {
            $_ => $self->get_value($_)->as_graph_value
        } $self->slot_names
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
