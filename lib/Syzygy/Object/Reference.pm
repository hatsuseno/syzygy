package Syzygy::Object::Reference;

use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ObjectReference
];

=head1 NAME

Syzygy::Object::Reference - Object reference base class

=head1 DESCRIPTION

=cut

use Syzygy::Types qw[ObjectType];

=head1 ATTRIBUTES

=head2 type

Reference to an object that implements L<Syzygy::Interface::ObjectType>.

=cut

# BEGIN wrapper for type_name requirement from ObjectReference role
BEGIN {
    has type => (
        is => 'ro',
        isa => ObjectType,
        required => 1,
        handles => {
            type_name => 'name'
        }
    );
}

=head2 id

String that uniquely identifies an object of the referenced L</type>.

=cut

# BEGIN wrapper for type_name requirement from ObjectReference role
BEGIN {
    has id => (
        is => 'rw',
        isa => 'Str',
        required => 1
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
