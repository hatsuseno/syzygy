package Syzygy::Object::Types::Service;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Syzygy::Object::Types::Service - Object type for C<service> instances

=head1 DESCRIPTION

This class declares the C<service> object type, which is used by
L<Zaaksysteem::Service::HTTP> services for informational purposes.

=cut

use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 repository

Links the repository where the service's authoritive codebase can be found.

=cut

szg_attr repository => (
    value_type_name => 'uri'
);

=head2 name

Human-readable name of the service.

=cut

szg_attr name => (
    value_type_name => 'string'
);

=head2 version

Version of the service

=cut

szg_attr version => (
    value_type_name => 'string'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
