package Syzygy::Object::Types::Set;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Syzygy::Object::Types::Set - Object type definition for C<set> instances.

=head1 DESCRIPTION

The class declares the Syzygy object type C<set>, which functions as a
generalised collection of objects.

=cut

use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 pager

Represents the set's state within a paging API.

=cut

szg_attr pager => (
    value_type_name => 'complex'
);

=head2 rows

An array of object instances.

=cut

szg_attr rows => (
    value_type_name => 'array'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
