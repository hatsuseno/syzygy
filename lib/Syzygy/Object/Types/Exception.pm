package Syzygy::Object::Types::Exception;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Syzygy::Object::Types::Exception - Generic exception type

=head1 DESCRIPTION

This class declares a generic C<exception> object type, which is used to
communicate error states on the level of the Syzygy runtime.

=cut

use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 type

A L<Syzygy::Types/Name> string defining the type of exception.

Good exceptions 'namespace' the type, using origin, action, and failure as
components. For example

    syzygy/object/create/name_conflict
    bttw/tools/signature/validation_failure

Using the above examples, it is relatively clear from just the type of
exception what went wrong.

=cut

szg_attr type => (
    value_type_name => 'type_name'
);

=head2 message

A C<text> attribute for a more human-readable explanation of the exception. 

Good messages include references to the environment where the error occured,
what object caused the error, and optionally what should be done to resolve
the issue.

=cut

szg_attr message => (
    value_type_name => 'text'
);

=head2 timestamp

UTC timestamp of the moment the exception occurred.

=cut

szg_attr timestamp => (
    value_type_name => 'datetime'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
