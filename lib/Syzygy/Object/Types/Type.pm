package Syzygy::Object::Types::Type;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Syzygy::Object::Types::Type - Generic type object type

=head1 DESCRIPTION

This class declares a generic C<type> object type, which is used to
communicate types from one service to another.

=cut

use BTTW::Tools;
use Syzygy::Object::Type;
use Syzygy::Syntax;
use Syzygy::Types qw[Name];

=head1 OBJECT TYPE ATTRIBUTES

=head2 type_name

=cut

szg_attr type_name => (
    value_type_name => 'type_name'
);

=head2 attributes

=cut

szg_attr attributes => (
    value_type_name => 'complex'
);

=head1 OBJECT METHODS

=head2 as_type

=cut

sig as_type => 'Syzygy::Object::Instance';

sub as_type {
    my $self = shift;
    my $instance = shift;

    my %attributes;

    if ($instance->has_value('attributes')) {
        my $attributes_spec = $instance->get_value('attributes')->value;

        %attributes = map {
            $_ => $self->_inflate_attribute_from_spec(
                $_,
                $attributes_spec->{ $_ }
            )
        } keys %{ $attributes_spec };
    }

    return Syzygy::Object::Type->new(
        name => $instance->get_value('type_name')->value,
        attributes => \%attributes
    );
}

=head1 PRIVATE METHODS

=head2 _inflate_attribute_from_spec

=cut

sub _inflate_attribute_from_spec {
    my $self = shift;
    my $name = shift;

    my $spec = assert_profile(shift, profile => {
        required => {
            value_type_name => Name
        }
    })->valid;

    my $model = Syzygy::Object::Model->get_instance;

    my $value_type_name = $spec->{ value_type_name };

    # Model attribute->type mapping bookkeeping, check existing mappings and
    # create one if it doesn't already exist.
    # TODO This logic is implicitly duplicated in Syzygy::Syntax, abstractify.
    if ($model->has_attribute_type_mapping($name)) {
        my $existing = $model->get_attribute_type_mapping($name);

        if ($existing ne $value_type_name) {
            throw('syzygy/object/type/attribute/value_type_conflict', sprintf(
                'Attribute "%s": previously declared type "%s" conflicts with "%s"',
                $name,
                $existing,
                $value_type_name
            ));
        }
    } else {
        $model->set_attribute_type_mapping($name, $value_type_name);
    }

    unless ($model->has_value_type($value_type_name)) {
        throw('syzygy/object/type/attribute/value_type_invalid', sprintf(
            'Attribute "%s": value type "%s" not found',
            $name,
            $value_type_name
        ));
    }
    
    return Syzygy::Object::Attribute->new(
        name => $name,
        value_type => $model->get_value_type($value_type_name)
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the C<LICENSE> file.
