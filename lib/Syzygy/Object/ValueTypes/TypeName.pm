package Syzygy::Object::ValueTypes::TypeName;

use Moose;
use namespace::autoclean;

with 'Syzygy::Interface::ValueType';

=head1 NAME

Syzygy::Object::ValueTypes::TypeName - Value type for the L<Syzygy::Types/Name>
type.

=head1 DESCRIPTION

This value type abstracts the concept of a name-string, as used internally by
Syzygy. TypeName strings define a (sub)type of a specific object. For
example: C<abc>, C<abc_xyz>, C<a123>, and C<abc/xyz_a123>.

TypeName strings include, optionally, a namespace (seperated by C</>).

=cut

use Syzygy::Types qw[Name];

=head1 METHODS

=head2 name

Returns the static type name for this value type (C<typename>).

=cut

sub name {
    return 'type_name';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return Name;
}

=head2 equal

Implements equality testing of two values in C<type_name> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && defined $b;
    return unless $a->has_value && $b->has_value;

    return $a->value eq $b->value;
}

=head2 not_equal

Implements inequality testing of two values in C<type_name> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 coerce

Implements value coercions from C<text> and C<string> values to C<type_name>
value instances.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    my $value_type_name = $value->type_name;

    if ($value_type_name eq 'string' || $value_type_name eq 'text') {
        return $self->new_value($value->value);
    }

    return;
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
