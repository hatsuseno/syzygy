package Syzygy::Object::ValueTypes::Boolean;

use Moose;
use namespace::autoclean;

with 'Syzygy::Interface::ValueType';

=head1 NAME

Syzygy::Object::ValueTypes::Boolean - L<Syzygy::Object> value type
for boolean values

=head1 DESCRIPTION

This class abstracts C<boolean> values.

=cut

use BTTW::Tools;

=head1 METHODS

=head2 name

Implements type name constant interface for
L<Syzygy::Interface::ValueType>.

Returns the static string C<boolean>.

=cut

sub name {
    return 'boolean';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return 'Bool';
}

=head2 equal

Implements equality testing of two values in C<boolean> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return $a->value == $b->value;
}

=head2 not_equal

Implements non-equality testing of two values in C<boolean> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 coerce

Returns a boolean-ish perl scalar derived from the provided
L<Syzygy::Object::Value>-implementing value.

B<Note>: this method assumes all defined values are true-ish, and all
undefined values are false-ish. This means the perl semantics of empty string
C<''>, C<int(0)>, and the empty list C<()> do not apply for values.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return $self->new_value($value->has_value);
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value ? \1 : \0;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
