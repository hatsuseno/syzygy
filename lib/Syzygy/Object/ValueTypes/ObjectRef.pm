package Syzygy::Object::ValueTypes::ObjectRef;

use Moose;
use namespace::autoclean;

with 'Syzygy::Interface::ValueType';

=head1 NAME

Syzygy::Object::ValueTypes::ObjectRef - Abstracts the notion of a
reference-to-an-object value

=head1 DESCRIPTION

This value type is used to pass around references to objects in the object
infrastructure.

It can be used to retrieve fully inflated objects, or relate objects to one
another.

=cut

use BTTW::Tools;
use List::Util qw[all];
use Moose::Util::TypeConstraints qw[role_type];
use Syzygy::Types qw[ObjectReference];

=head1 METHODS

=head2 name

Returns the static value type string C<object_ref>.

=cut

sub name {
    return 'object_ref'
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return ObjectReference;
}

=head2 equal

Implements equality testing for values in C<object_ref> context.

=cut

sub equal {
    my $self = shift;
    my $a = shift;
    my $b = shift;

    # a and b must be object_ref values
    return unless $a->type_name eq $self->name && $b->type_name eq $self->name;

    # object refs in a and b must be equal
    return unless $a->value->type eq $b->value->type;
    return unless $a->value->id eq $b->value->id;

    return 1;
}

=head2 not_equal

Implements non-equality testing for values in C<object_ref> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 coerce

Stub implementation of L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce { }

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return {
        type => $value->value->type_name,
        id => $value->value->id,
        instance => undef
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
