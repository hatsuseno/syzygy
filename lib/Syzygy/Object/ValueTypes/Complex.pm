package Syzygy::Object::ValueTypes::Complex;

use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ValueType
];

=head1 NAME

Syzygy::Object::ValueTypes::Complex - C<complex> value type class and
behavior for L<Syzygy::Object>.

=head1 DESCRIPTION

=cut

use BTTW::Tools;

=head1 METHODS

=head2 name

Implements L<Syzygy::Interface::ValueType/name>.

=cut

sub name {
    return 'complex';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return 'HashRef';
}

=head2 coerce

Stub implementation of L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce { }

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
