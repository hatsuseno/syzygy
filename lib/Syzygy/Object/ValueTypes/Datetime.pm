package Syzygy::Object::ValueTypes::Datetime;

use Moose;
use namespace::autoclean;

with 'Syzygy::Interface::ValueType';

=head1 NAME

Syzygy::Object::ValueTypes::Datetime - C<datetime> value instances
and behavior for L<Syzygy::Object>s.

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use DateTime;
use DateTime::Format::ISO8601;

=head1 METHODS

=head2 name

Implements type name constant interface for L<Syzygy::Interface::ValueType>.

Returns the static string C<datetime>.

=cut

sub name {
    return 'datetime';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return 'DateTime';
}

=head2 compare

Implements a generic comperator for C<datetime> values.

Returns whatever L<DateTime/compare> returns.

=cut

sub compare {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && defined $b;

    return DateTime->compare($a->value, $b->value);
}

=head2 equal

Implements equality testing of two values in C<datetime> context.

=cut

sub equal {
    return shift->compare(@_) == 0;
}

=head2 not_equal

Implements non-equality testing of two values in C<datetime> context.

=cut

sub not_equal {
    return shift->compare(@_) != 0;
}

=head2 less_than

Implements less-than testing of two values in C<datetime> context.

=cut

sub less_than {
    return shift->compare(@_) < 0;
}

=head2 greater_than

Implements greater-than testing of two values in C<datetime> context.

=cut

sub greater_than {
    return shift->compare(@_) > 0;
}

=head2 equal_or_less_than

Implements equal-or-less-than testing of two values in C<datetime> context.

=cut

sub equal_or_less_than {
    return shift->compare(@_) <= 0;
}

=head2 equal_or_greater_than

Implements equal-or-greater-than testing of two values in C<datetime> context.

=cut

sub equal_or_greater_than {
    return shift->compare(@_) >= 0;
}

=head2 coerce

Implements L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    if ($value->type_name eq 'string') {
        return $self->new_value($self->parse_datetime_string($value->value));
    }
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return sprintf('%sZ', $value->value->iso8601);
}

=head2 parse_datetime_string

Datetime parser for string datetime values.

    my $dt = $type->parse_datetime_string('2017-01-01T12:34:56Z');

=cut

sub parse_datetime_string {
    my $self = shift;
    my $value = shift;

    # DateTime warns and moans when a parse failure occurs, silence using eval
    my $dt = eval { DateTime::Format::ISO8601->parse_datetime("$value") };

    return $dt if defined $dt;

    throw('syzygy/object/value_type/datetime/validation_error', sprintf(
        'Attempted to coerce "%s" as datetime, cannot parse or invalid datetime',
        $value
    ));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
