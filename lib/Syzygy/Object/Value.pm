package Syzygy::Object::Value;

use Moose;
use namespace::autoclean;

=head1 NAME

Syzygy::Object::Value - Abstracted object value class

=head1 DESCRIPTION

The object infrastructure uses boxed/wrapped/abstracted notions of values,
this class is a data holder for such values.

=cut

use Syzygy::Types qw[ValueType];

=head1 ATTRIBUTES

=head2 type

Reference to a L<Syzygy::Interface::ValueType>-implementing object.

=head3 Delegates

=over 4

=item type_name

See L<Syzygy::Interface::ValueType/name>.

=back

=cut

has type => (
    is => 'ro', # ro because values should be immutable.
    isa => ValueType,
    required => 1,
    handles => {
        type_name => 'name',
    }
);

=head2 value

Holder of the Perl-level value abstracted by an instance of this class.

=head3 Delegates

=over 4

=item has_value

Predicate that returns true if any value is set (including C<undef>).

=back

=cut

has value => (
    is => 'ro', # ro because we don't trigger validation for this accessor
    predicate => 'has_value'
);

=head1 METHODS

=head2 as_graph_value

This method returns a deflated representation of the value, as returned by the
type's L<deflate_value|Syzygy::Interface::ValueType/deflate_value> method.

If L</value> is undefined, the return value of this method will also be
undefined (empty list).

=cut

sub as_graph_value {
    my $self = shift;

    return unless $self->has_value;

    return $self->type->deflate_value($self);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
