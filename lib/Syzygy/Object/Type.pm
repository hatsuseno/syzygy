package Syzygy::Object::Type;

use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ObjectType
];

=head1 NAME

Syzygy::Object::Type - Dynamic implementation of
L<Syzygy::Interface::ObjectType>.

=head1 DESCRIPTION

This class implements a dynamic object type, that can be configured at
run-time.

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name];

=head1 ATTRIBUTES

=head2 name

L<Name|Syzygy::Types/Name> of the type.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has name => (
        is => 'rw',
        isa => Name,
        required => 1
    );
}

=head2 attributes

Map of L<Syzygy::Object::Attribute> instances associated with the type.

=head3 Delegates

=over 4

=item has_attribute

Predicate for existance checks for a given attribute name

=item get_attribute

Getter for attribute instances

=item set_attribute

Setter for attribute instances.

=item all_attributes

Returns a list of attribute instances.

=item attribute_names

Returns a list of attribute names.

=back

=cut

# BEGIN, so role application has the interface
BEGIN {
    has attributes => (
        is => 'rw',
        isa => 'HashRef[Syzygy::Object::Attribute]',
        traits => [qw[Hash]],
        default => sub { return {} },
        handles => {
            has_attribute => 'exists',
            get_attribute => 'get',
            set_attribute => 'set',
            all_attributes => 'values',
            attribute_names => 'keys',
        }
    );
}

=head2 ignore_unknown_attributes

Implements L<Syzygy::Interface::ObjectType/ignore_unknown_attributes>.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has ignore_unknown_attributes => (
        is => 'rw',
        isa => 'Bool',
        default => 0
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
