package Syzygy::Object::Source;

use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::Source
];

=head1 NAME

Syzygy::Object::Source - Binding a L<Syzygy::Interface::Store> to a name

=head1 DESCRIPTION

This class adds some metadata to any store, for use in
L<Syzygy::Object::Model>.

Stores, for example, have no inherent name, or mode of access specification.

=head1 SYNOPSIS

    my $source = Syzygy::Object::Source->new(
        store => Syzygy::Store::Simple->new,
        name => 'my_simple_store',
        mode => 'read-only'
    );

    if ($source->is_writable) {
        ...
    }

=cut

use BTTW::Tools;
use Syzygy::Types qw[AccessMode Name Store];

=head1 ATTRIBUTES

=head2 name

Implements L<Syzygy::Interface::Source/name>.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has name => (
        is => 'rw',
        isa => Name,
        required => 1
    );
}

=head2 metadata

Implements L<Syzygy::Interface::Source/metadata>.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has metadata => (
        is => 'rw',
        isa => 'HashRef',
        predicate => 'has_metadata'
    );
}

=head2 mode

Implements L<Syzygy::Interface::Source/mode>.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has mode => (
        is => 'rw',
        isa => AccessMode,
        default => 'read-write'
    );
}

=head2 store

Implements L<Syzygy::Interface::Source/store>.

=cut

# BEGIN, so role application has the interface
BEGIN {
    has store => (
        is => 'rw',
        isa => Store,
        required => 1
    );
}

=head1 METHODS

=head2 is_writable

Returns true-ish when L</mode> is set to C<read-write>.

=cut

sub is_writable {
    return shift->mode eq 'read-write';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
