package Syzygy::Object::Attribute;

use Moose;
use namespace::autoclean;

=head1 NAME

Syzygy::Object::Attribute - Object attribute bindings

=head1 DESCRIPTION

This class is used to bind L<Syzygy::Interface::ValueType> instances to
L<Syzygy::Interface::ObjectType> instances.

=cut

use BTTW::Tools;
use Syzygy::Types qw[AttributeNamespace Name ValueType];

=head1 ATTRIBUTES

=head2 name

L<Name|Syzygy::Types/Name> of the attribute.

=cut

has name => (
    is => 'rw',
    isa => Name,
    required => 1
);

=head2 value_type

L<ValueType|Syzygy::Types/ValueType> of the attribute.

=cut

has value_type => (
    is => 'rw',
    isa => ValueType,
    required => 1
);

=head2 namespace

Namespace of the attribute. See
L<AttributeNamespace|Syzygy::Types/AttributeNamespace> for details.

=cut

has namespace => (
    is => 'rw',
    isa => AttributeNamespace,
    default => 'sys'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
