package Syzygy::Interface::MooseAttribute;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Syzygy::Interface::MooseAttribute - L<Moose::Meta::Attribute> integration trait

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name ValueType];

=head1 ATTRIBUTES

=head2 szg_name

Required name of the attribute, in the context of the Syzygy object.

=cut

has szg_name => (
    is => 'ro',
    isa => Name,
    required => 1
);

=head2 label

Optional label for the attribute.

=cut

has label => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_label'
);

=head2 value_type

Required reference to an object implementing L<Syzygy::Interface::ValueType>.

=cut

has value_type => (
    is => 'ro',
    isa => ValueType,
    required => 1
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
