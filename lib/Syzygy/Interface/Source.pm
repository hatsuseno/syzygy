package Syzygy::Interface::Source;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools qw[sig];

use Syzygy::Types qw[Name Store AccessMode];

=head1 NAME

Syzygy::Interface::Source - Interface declaration for object sources

=head1 DESCRIPTION

This role defines the concept of an 'object source' for the Syzygy library.
Sources are abstractions of a named L<Syzygy::Interface::Store> with some
additional metadata (like writability).

=head1 INTERFACE

=head2 name

Must return a L<Syzygy::Types/Name>-compliant name for the source.

=head2 store

Sources require an object which implements L<Syzygy::Interface::Store>.

=head2 mode

Required specification of the L<access mode|Syzygy::Types/AccessMode> of the
source.

=head2 metadata

Should return a C<HashRef> of metadata fields (specification of store,
run-time variables, etc).

=cut

requires qw[
    name
    store
    metadata
    mode
];

#sig name => '=> Name';
# sig does not support roles or MooseX scoped type constraints
around name => _check_retval(Name);

#sig store => '=> Store'
# sig does not support roles or MooseX scoped type constraints
around store => _check_retval(Store);

sig metadata => '=> ?HashRef';

#sig mode => '=> AccessMode';
# sig does not support roles or MooseX scoped type constraints
around mode => _check_retval(AccessMode);

=head1 PRIVATE FUNCTIONS

=head2 _check_retval

Helper that produces a bare-bones L<BTTW::Tools/sig> equivalent for
interfaces. Cannot handle list returns, only one constraint at a time, etc.

=cut

sub _check_retval {
    my $constraint = shift;

    return sub {
        my $orig = shift;
        my $self = shift;

        my $retval = $self->$orig(@_);

        $constraint->assert_valid($retval);

        return $retval;
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
