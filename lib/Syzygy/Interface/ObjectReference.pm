package Syzygy::Interface::ObjectReference;

use Moose::Role;

=head1 NAME

Syzygy::Interface::ObjectReference - Interface role for object references

=head1 DESCRIPTION

This role specifies the interface requires for basic Syzygy object references

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name UUID];

=head1 INTERFACE

=head2 type_name

Requires a L<name|Syzygy::Types/Name> for the referenced object's type.

=head2 id

Requires a L<UUID|Syzygy::Types/UUID> for the referenced object.

=cut

requires qw[
    type_name
    id
];

#sig does not support MooseX::Types: sig type_name => '=> Name';
around type_name => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_type_name = $self->$orig(@_);

    Name->assert_valid($maybe_type_name);

    return $maybe_type_name;
};

#sig does not support MooseX::Types: sig id => '=> UUID';
around id => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_id = $self->$orig(@_);

    UUID->assert_valid($maybe_id);

    return $maybe_id;
};

=head1 PRIVATE METHODS

=head2 _ref

Internal reference instance builder.

=cut

sub _ref {
    my $self = shift;

    return $self->new(
        type_name => $self->type_name,
        id => $self->id
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
