package Syzygy::Interface::ValueType;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Syzygy::Interface::ValueType - Common API for L<Syzygy::Object> values.

=head1 DESCRIPTION

This interface spdecifies the required methods for object type instances.

=cut

use BTTW::Tools;
use Moose::Util::TypeConstraints;
use Syzygy::Object::Value;
use Syzygy::Types qw[Name Value];

=head1 REQUIRED INTERFACES

=head2 name

Requires a static name method.

=head2 perl_type_constraint

Used during value construction from perl context. The value will be checked
using the provided constraint, and will be assumed legal content of the value
object if the value validates.

=head2 coerce

Used during value construction from Syzygy context. The value provided value
must either be of the same type as the type being coerced to, or the type
needs explicit support for the coercion.

=head2 deflate_value

Value types are expected to know how to flatten values of their type to a
serializable value.

Often this will be a simple redispatch of the value's value field, but this
may not be the case for, for example, object reference values and other
non-scalar data.

The method is called with a L<Syzygy::Object::Value> instance as argument,
which is guaranteed to have a value (no need to check
C<< $value->has_value >>).

=cut

requires qw[
    name
    perl_type_constraint
    coerce
    deflate_value
];

#sig does not support MooseX::Types: sig name => '=> Name';
around name => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_name = $self->$orig(@_);

    Name->assert_valid($maybe_name);

    return $maybe_name;
};

#prevent around(around(around())) overhead
#sig coerce => 'Syzygy::Object::Value => ?Syzygy::Object::Value';
around coerce => sub {
    my $orig = shift;
    my $self = shift;
    my $value = shift;

    Value->assert_valid($value);

    # Default rule for coercions, if the type is already the target type,
    # just return.
    return $value if $value->type_name eq $self->name;

    my $maybe_value = $self->$orig($value);

    Value->assert_valid($maybe_value) if defined $maybe_value;

    return $maybe_value;
};

# ensure deflate_value can always rely on receiving an actual value for
# deflating
sig deflate_value => 'Syzygy::Object::Value';

=head1 METHODS

=head2 new_value

Builds a new L<Syzygy::Object:Value> instance using the provided value data.

This method tries to be semi-smart by introspecting the supplied value for
it's parent. If the value is already a value instance, the L</must_coerce>
logic will be used.

    # Perl data is checked via L</perl_type_constraint>.
    my $foo = $type->new_value('abc');

    # Syzygy values are coerced via L</coerce>.
    my $bar = $type->new_value(szg_value('text', 'abc'));

=cut

sub new_value {
    my $self = shift;
    my $value = shift;

    # Unpack existing value objects if detected.
    if (eval { $value->isa('Syzygy::Object::Value') }) {
        # Same type, assume value is valid and create new value instance
        if ($value->type_name eq $self->name) {
            return Syzygy::Object::Value->new(
                type => $self,
                value => $value->value
            );
        }

        # Coercion must succeed
        return $self->must_coerce($value);
    }

    my $tc = $self->_moose_type_constraint;

    if ($tc->has_coercion) {
        $value = $tc->coerce($value);
    }

    unless ($tc->check($value)) {
        throw('syzygy/object/type/constraint_failure', sprintf(
            'Attempted to use "%s" as %s value',
            $value // '<undef>',
            $self->name
        ));
    }

    return Syzygy::Object::Value->new(
        type => $self,
        value => $value
    );
}

=head2 must_coerce

Calls L</coerce> and asserts a value was returned.

Throws C<syzygy/object/value_type/value_types_not_compatible> if the assertion
fails.

=cut

sub must_coerce {
    my $self = shift;
    my $value = shift;

    my $retval = $self->coerce($value);

    return $retval if defined $retval;

    throw('syzygy/object/value_type/value_types_not_compatible', sprintf(
        'No coercion from type "%s" to "%s"',
        $value->type_name,
        $self->name
    ));
}

=head1 PRIVATE METHODS

=head2 _moose_type_constraint

Attempts to return a L<Moose::Meta::TypeConstraint> instance using.

This method is used in context of L</perl_type_constraint>, since
implementations may choose to return a Moose type constraint string (
C<HashrRef>, C<ClassName>, C<HashRef[Str|Num]>, etc) or a constraint
instance.

=cut

sub _moose_type_constraint {
    my $self = shift;

    my $maybe_constraint = $self->perl_type_constraint;

    unless (defined $maybe_constraint) {
        throw('syzygy/object/value_type/constraint_invalid', sprintf(
            'Value type "%s" returned undefined perl type constraint',
            $self->name
        ));
    }

    return $maybe_constraint if eval {
        $maybe_constraint->isa('Moose::Meta::TypeConstraint')
    };

    return Moose::Util::TypeConstraints::find_or_create_isa_type_constraint(
        $maybe_constraint
    );
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
