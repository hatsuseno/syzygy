package Syzygy;

use strict;
use warnings;

our $VERSION = '0.002';

=head1 NAME

Syzygy - Dynamic object interaction library

=head1 DESCRIPTION

Syzygy is a set of libraries used for defining, storing, searching, and
interacting with objects.

Syzygy differentiates itself from, for example, L<Moose> because it works with
fully dynamic object types and instances. At the cost of speed, of course.

The project is still heavily under development, and should be considered beta
quality at best.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
