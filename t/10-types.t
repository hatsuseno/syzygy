#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::Mock::One;

use Syzygy::Types qw[:all];
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

sub mock {
    return Test::Mock::One->new(@_);
}

sub mock_obj {
    return mock('X-Mock-ISA' => ['Moose::Object'], @_);
}

my %tests = (
    name => {
        type => Name,
        ok => [qw[foo foo_bar foo123bar foo123_bar foo_123bar foo/bar foo/bar/baz a/b/c]],
        fail => ['', qw[123foo FooBar Foo_Bar _bar _123 <> ; - = /foo bar/ foo//bar a/_b a_/b]]
    },
    access_mode => {
        type => AccessMode,
        ok => [qw[read-write read-only]],
        fail => [qw[write-only abc foo _ 123]]
    },
    uuid => {
        type => UUID,
        ok => [
            (map { create_uuid_as_string(UUID_V4) } 1..10),
            '9ffc1aa5-4749-9237-995d-4a34258dfe18',
            '00000000-0000-0000-0000-000000000000'
        ],
        fail => [
            '',
            '123',
            '1-b-c-d-e',
            create_uuid_as_string(UUID_V4) . 'a',
        ]
    },
    object_type => {
        type => ObjectType,
        ok => [ mock_obj(does => sub { shift eq 'Syzygy::Interface::ObjectType' }) ],
        fail => [ qw[123 foo], mock(does => sub { 0 }) ]
    },
    source => {
        type => Source,
        ok => [ mock_obj(does => sub { shift eq 'Syzygy::Interface::Source' }) ],
        fail => [ qw[123 foo], mock(does => sub { 0 }) ]
    },
    store => {
        type => Store,
        ok => [ mock_obj(does => sub { shift eq 'Syzygy::Interface::Store' }) ],
        fail => [qw[123 foo], mock(does => sub { 0 }) ]
    },
    value => {
        type => Value,
        ok => [ mock('X-Mock-ISA' => [qw[Moose::Object Syzygy::Object::Value]]) ],
        fail => [qw[123 foo], mock_obj() ]
    },
    value_type => {
        type => ValueType,
        ok => [ mock_obj(does => sub { shift eq 'Syzygy::Interface::ValueType' }) ],
        fail => [qw[123 foo], mock(does => sub { 0 }) ]
    }
);

for my $name (keys %tests) {
    my $type = $tests{ $name }{ type };

    ok $type->assert_valid($_), sprintf('"%s" is a valid %s', $_, $name) for @{
        $tests{ $name }{ ok }
    };

    dies_ok {
        $type->assert_valid($_)
    } sprintf('"%s" not a valid %s', $_, $name) for @{
        $tests{ $name }{ fail }
    };
}

done_testing;

1;
