package Syzygy::Test::Object;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Syntax;

use Syzygy::Object;
use Syzygy::Object::Attribute;
use Syzygy::Object::Type;
use Syzygy::Object::ValueTypes::String;

sub test_object {
    my $string_value_type = Syzygy::Object::ValueTypes::String->new;

    my $type = Syzygy::Object::Type->new(
        name => 'foo',
        attributes => {
            foo => Syzygy::Object::Attribute->new(name => 'foo', value_type => $string_value_type),
            bar => Syzygy::Object::Attribute->new(name => 'bar', value_type => $string_value_type)
        }
    );

    my $type_required_error = exception { Syzygy::Object->new };

    isa_ok $type_required_error, 'Moose::Exception::AttributeIsRequired',
        'missing attribute object instantiation retval';

    is $type_required_error->attribute_name, 'type', 'type attribute required';

    lives_ok { Syzygy::Object->new(type => $type) } 'empty object instantiation';

    my $foo = Syzygy::Object->new(type => $type);

    isa_ok $foo, 'Syzygy::Object', 'object instantiator retval';

    ok !$foo->has_instance, 'empty object has no instance';

    lives_ok { $foo->set_value('foo', 'bar') } 'allowed to set value on empty object';

    ok $foo->has_instance, 'object instantiated after setting value';

    is_deeply $foo->as_graph_hash, {
        type => $type->name,
        reference => undef,
        instance => {
            foo => 'bar'
        }
    }, 'object graph_hash returns deflated view of object';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
