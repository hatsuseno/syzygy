package Syzygy::Test::Syntax;

use Test::Class::Moose;
use Test::Most;
use Test::Fatal;

use Syzygy::Syntax;

=head1 NAME

Syzygy::Test::Syntax - Syzygy syntax helper tests

=head1 SYNOPSIS

    prove -l -v :: Syzygy::Test::Syntax

=cut

sub test_syzygy_helpers {
    ok defined &szg_attr, 'Syzygy::Syntax exports szg_attr()';
    ok defined &szg_value, 'Syzygy::Syntax exports szg_value()';
    ok defined &szg_object_ref, 'Syzygy::Syntax exports szg_object_ref()';

    ok defined &qb, 'Syzygy::Syntax exports qb() (assuming this means all of Syzygy::Query)';
}

sub test_syzygy_attr {
    my $meta = Syzygy::Test::Syntax::LocalPkg->meta;

    my $untyped_error = exception {
        Syzygy::Syntax::szg_attr($meta, 'abc');
    };

    isa_ok $untyped_error, 'BTTW::Exception::Base', 'szg_attr: adding without value type excepts';
    is $untyped_error->type, 'syzygy/object/attribute/value_type_name_missing',
        'szg_attr: adding without value type exception type';

    my $invalid_name_error = exception {
        Syzygy::Syntax::szg_attr($meta, '123');
    };

    isa_ok $invalid_name_error, 'BTTW::Exception::Base', 'szg_attr: adding with invalid name excepts';
    is $invalid_name_error->type, 'syzygy/object/attribute/name_invalid',
        'szg_attr: adding with invalid name exception type';

    lives_ok {
        Syzygy::Syntax::szg_attr($meta, abc => (
            value_type_name => 'string'
        ));
    } 'szg_attr: adding attribute with value type lives';

    ok !$meta->has_attribute('abc'), 'szg_attr: meta class does not have like-named attribute';
    ok $meta->has_attribute('_szg_attr_abc'), 'szg_attr: meta class does have syzygy internal attribute';

    ok eval {
        $meta->get_attribute('_szg_attr_abc')->does('Syzygy::Interface::MooseAttribute')
    }, 'szg_attr: meta class has attribute with expected attached trait';

    my $redefine_error = exception {
        Syzygy::Syntax::szg_attr($meta, abc => ( value_type_name => 'text' ));
    };

    isa_ok $redefine_error, 'BTTW::Exception::Base', 'szg_attr: redefining value type for know attribute';
    is $redefine_error->type, 'syzygy/object/attribute/value_type_conflict',
        'szg_attr: redefining known attribute type mapping error';
}

package Syzygy::Test::Syntax::LocalPkg {
    use Moose;
    use Syzygy::Syntax;

    extends 'Syzygy::Object::Type::Moose';
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
