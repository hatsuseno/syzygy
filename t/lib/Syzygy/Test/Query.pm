package Syzygy::Test::Query;

use Test::Class::Moose;
use Test::Most;

use Syzygy::Query;

=head1 NAME

Syzygy::Test::Query - Object query tests

=head1 SYNOPSIS

    prove -l -v :: Syzygy::Test::Query

=cut

sub test_helpers {
    my $query = qb('case');

    isa_ok($query, 'Syzygy::Query', 'qb() helper produces query instance');
    is($query->type, 'case', 'qb("type") sets correct type');
    ok(!$query->has_cond, 'qb() with single parameter has no condition');
    ok(!$query->has_sort, 'qb() with single parameter has no sort');

    is_deeply $query->as_complex_value, {
        type_name => 'case',
        cond => undef,
        sort => undef
    }, 'as_complex_value returns expected data';
}

sub test_qb_eq {
    my $expr = qb_eq('field', 'value');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Comparison',
    );

    my @exprs = $expr->all_expressions;

    ok(scalar(@exprs) == 2, 'equality produces expression instances');

    isa_ok(
        $exprs[0],
        'Syzygy::Query::Expression::Field',
        'eq->expressions[0]'
    );

    isa_ok(
        $exprs[1],
        'Syzygy::Query::Expression::Literal',
        'eq->expressions[0]'
    );

    is $expr->stringify, join(' equal ', map { $_->stringify } @exprs),
        'equal comparison stringification';

    is_deeply $expr->as_complex_value, {
        comparison => {
            mode => $expr->mode,
            expressions => [
                map { $_->as_complex_value } $expr->all_expressions
            ]
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_and {
    my $expr = qb_and();

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Conjunction',
    );

    ok(scalar($expr->all_expressions) == 0, 'no arguments give no expressions');

    $expr = qb_and(qb_eq('foo', 'value'), qb_eq('bar', 'value'));

    ok(scalar($expr->all_expressions) == 2, 'arguments give expressions');

    is $expr->stringify, join(
        ' and ',
        map { $_->stringify } $expr->all_expressions
    ), 'and conjunction stringification';

    is_deeply $expr->as_complex_value, {
        conjunction => {
            expressions => [
                map { $_->as_complex_value } $expr->all_expressions
            ]
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_or {
    my $expr = qb_or();

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Disjunction',
    );

    ok(scalar($expr->all_expressions) == 0, 'no arguments give no expressions');

    $expr = qb_or(qb_eq('foo', 'value'), qb_eq('bar', 'value'));

    ok(scalar($expr->all_expressions) == 2, 'arguments give expressions');

    is $expr->stringify, join(
        ' or ',
        map { $_->stringify } $expr->all_expressions
    ), 'and conjunction stringification';

    is_deeply $expr->as_complex_value, {
        disjunction => {
            expressions => [
                map { $_->as_complex_value } $expr->all_expressions
            ]
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_not {
    my $expr = qb_not(qb_and());

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Inversion',
    );

    isa_ok(
        $expr->expression,
        'Syzygy::Query::Expression::Conjunction',
        'not->expression'
    );

    is $expr->stringify, sprintf('not %s', $expr->expression->stringify),
        'logical inversion stringification';

    is_deeply $expr->as_complex_value, {
        inversion => {
            expression => $expr->expression->as_complex_value
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_in {
    my $expr = qb_in('field', []);

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::MemberRelation',
    );

    isa_ok(
        $expr->expression,
        'Syzygy::Query::Expression::Field',
        'in->expression'
    );

    isa_ok(
        $expr->set,
        'Syzygy::Query::Expression::Set',
        'in->set'
    );

    is $expr->stringify, sprintf(
        '%s is member of %s',
        $expr->expression->stringify,
        $expr->set->stringify
    ), 'member relation stringification';

    is_deeply $expr->as_complex_value, {
        member_relation => {
            set => $expr->set->as_complex_value,
            expression => $expr->expression->as_complex_value
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_ne {
    my $expr = qb_ne('field', 'value');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Comparison',
        'ne->expression'
    );

    is $expr->mode, 'not_equal', 'new->comparsion_mode';

    is $expr->stringify, join(
        ' not_equal ',
        map { $_->stringify } $expr->all_expressions
    ), 'not_equal comparision stringification';

    is_deeply $expr->as_complex_value, {
        comparison => {
            mode => $expr->mode,
            expressions => [
                map { $_->as_complex_value } $expr->all_expressions
            ]
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_cmp {
    my %tests = (
        equal => qb_eq('field', 'value'),
        not_equal => qb_ne('field', 'value'),
        less_than => qb_lt('field', 'value'),
        greater_than => qb_gt('field', 'value'),
        equal_or_less_than => qb_elt('field', 'value'),
        equal_or_greater_than => qb_egt('field', 'value')
    );

    for my $mode (keys %tests) {
        isa_ok $tests{ $mode }, 'Syzygy::Query::Expression::Comparison',
            sprintf('%s->expression type', $mode);

        is $tests{ $mode }->mode, $mode, sprintf('%s->cmp->mode', $mode);

        isa_ok $tests{ $mode }->expressions->[0], 'Syzygy::Query::Expression::Field';
        isa_ok $tests{ $mode }->expressions->[1], 'Syzygy::Query::Expression::Literal';

        is $tests{ $mode }->stringify, join(
            " $mode ",
            map { $_->stringify } $tests{ $mode }->all_expressions
        ), 'comparsion expression stringification';

        is_deeply $tests{ $mode }->as_complex_value, {
            comparison => {
                mode => $tests{ $mode }->mode,
                expressions => [
                    map { $_->as_complex_value } $tests{ $mode }->all_expressions
                ]
            }
        }, 'as_complex returns expected data';
    }
}

sub test_qb_like {
    my $expr = qb_like('field', 'value');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::ContainsString',
    );

    isa_ok(
        $expr->string,
        'Syzygy::Query::Expression::Field',
        'like->string'
    );

    isa_ok(
        $expr->match,
        'Syzygy::Query::Expression::Literal',
        'like->match'
    );

    is($expr->mode, 'infix', 'default substr match is infix');

    $expr = qb_like('field', 'value', 'prefix');

    is($expr->mode, 'prefix', 'prefix substr match mode can be set');

    is $expr->stringify, sprintf(
        '%s match %s to %s',
        $expr->mode,
        $expr->match->stringify,
        $expr->string->stringify
    ), 'like comparison stringification';

    is_deeply $expr->as_complex_value, {
        contains_string => {
            mode => $expr->mode,
            match => $expr->match->as_complex_value,
            string => $expr->string->as_complex_value
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_re {
    my $expr = qb_re('field', 'regex');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Regex'
    );

    isa_ok(
        $expr->string,
        'Syzygy::Query::Expression::Field',
        'String argument interpreted as field'
    );

    is($expr->pattern, 'regex', 'String argument interpreted as pattern');

    is $expr->stringify, sprintf(
        '%s matches /%s/',
        $expr->string->stringify,
        $expr->pattern
    ), 'regex comparison stringify';

    is_deeply $expr->as_complex_value, {
        regex => {
            string => $expr->string->as_complex_value,
            pattern => $expr->pattern
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_field {
    my $expr = qb_field('field');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Field',
    );

    is($expr->name, 'field', 'String argument interpreted as fieldname');

    is $expr->stringify, sprintf('field:%s', $expr->name),
        'field stringification';

    is_deeply $expr->as_complex_value, {
        field => { name => $expr->name }
    }, 'as_complex_value returns expected data';
}

sub test_qb_lit {
    my $expr = qb_lit('type', 'value');

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Literal',
    );

    is($expr->type, 'type', 'String argument interpreted as value type');
    is($expr->value, 'value', 'String argument interpreted as value');

    is $expr->stringify, sprintf('%s(%s)', $expr->type, $expr->value),
        'typed value literal stringification';

    is_deeply $expr->as_complex_value, {
        literal => {
            value_type_name => $expr->type,
            value => $expr->value
        }
    }, 'as_complex_value returns expected data';
}

sub test_qb_set {
    my $expr = qb_set();

    isa_ok(
        $expr,
        'Syzygy::Query::Expression::Set'
    );

    ok(scalar($expr->all_expressions) == 0, 'empty set has no expressions');

    $expr = qb_set(qb_lit('type', 'value'), qb_lit('type', 'other'));

    ok(scalar($expr->all_expressions) == 2, 'two literals == two expressions');

    is $expr->stringify, sprintf(
        '[ %s ]',
        join(', ', map { $_->stringify } $expr->all_expressions)
    ), 'set literal stringification';

    is_deeply $expr->as_complex_value, {
        set => [
            map { $_->as_complex_value } $expr->all_expressions
        ]
    }, 'as_complex_value returns expected data';
}

sub test_qb_sort {
    my $expr = qb_sort('field');

    isa_ok(
        $expr,
        'Syzygy::Query::Sort',
    );

    isa_ok(
        $expr->expression,
        'Syzygy::Query::Expression::Field',
        'String argument interpreted as field reference'
    );

    ok(!$expr->reverse, 'Default sort is non-inverted');

    $expr = qb_sort('field', 'desc');

    ok($expr->reverse, 'String argument inverts sort');

    is $expr->stringify, sprintf(
        '%s, reverse order',
        $expr->expression->stringify
    ), 'sort expression stringification';

    is_deeply $expr->as_complex_value, {
        sort => {
            direction => 'reverse',
            expression => $expr->expression->as_complex_value
        }
    }, 'as_complex_value returns expected data';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
