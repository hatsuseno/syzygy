package Syzygy::Test::Object::Type;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Syntax;

use Syzygy::Object::Type;
use Syzygy::Object::Attribute;
use Syzygy::Object::ValueTypes::String;
use Syzygy::Object::ValueTypes::Datetime;

sub test_object_type {
    my $type;

    lives_ok {
        $type = Syzygy::Object::Type->new(
            name => 'foo',
            attributes => {
                foo => Syzygy::Object::Attribute->new(
                    name => 'foo',
                    value_type => Syzygy::Object::ValueTypes::String->new
                )
            }
        );
    } 'creating simple type lives';

    isa_ok $type, 'Syzygy::Object::Type', 'object type constructor retval';

    ok $type->has_attribute('foo'), 'object type has foo attribute';

    my $invalid_name = exception { Syzygy::Object::Type->new(name => 'ABC') };

    isa_ok $invalid_name, 'Moose::Exception::ValidationFailedForInlineTypeConstraint',
        'invalid name in constructor throws error';

    is $invalid_name->attribute_name, 'name',
        'exception applies to expected field';

    my $attr_values = $type->process_attribute_values({
        foo => 'abc'
    });

    is ref $attr_values, 'HASH', 'process_attribute_values returned hashref';

    ok exists $attr_values->{ foo }, 'process_attribute_values returned expected key';

    isa_ok $attr_values->{ foo }, 'Syzygy::Object::Value',
        'process_attribute_values instantiated value object for plain string input';

    is $attr_values->{ foo }->type_name, 'string',
        'object value object has expected type';

    is $attr_values->{ foo }->value, 'abc',
        'object value object has expected value';

    my $maybe_empty_hash;

    lives_ok {
        $maybe_empty_hash = $type->process_attribute_values({ foo => undef });
    } 'processing attributes with empty value for non-required attribute lives';

    is_deeply $maybe_empty_hash, {}, 'undef value processing gets unset';

    my $unknown_value = exception { $type->process_attribute_values({ bar => 'baz' }) };

    isa_ok $unknown_value, 'BTTW::Exception::Base',
        'process_attribute_values throws bttw exception for unexpected input';

    is $unknown_value->type, 'syzygy/object/type/instance_values_validation_error',
        'process_attribute_values throws bttw exception of expected type';

    ok defined $unknown_value->object, 'exception has embedded object';
    is ref $unknown_value->object, 'HASH', 'embedded object is hashref';
    ok exists $unknown_value->object->{ bar }, 'embedded object has expected key';

    my $bar_error = $unknown_value->object->{ bar };

    is ref $bar_error, 'HASH', 'type->attribute validation returns hashref';
    ok exists $bar_error->{ error }, 'type->attribute validation hashref has error key';
    is $bar_error->{ error }, 'unknown_attribute', 'error of correct type';

    my $invalid_value = exception { $type->process_attribute_values({ foo => \1 }) };

    isa_ok $invalid_value, 'BTTW::Exception::Base',
        'process_attribute_values throws bttw exception for invalid input';

    ok defined $invalid_value->object,
        'process_attribute_values exception has object for invalid input';

    is ref $invalid_value->object, 'HASH',
        'process_attribute_values exception object isa hashref';

    my $invalid_error = $invalid_value->object->{ foo };

    ok defined $invalid_error, 'exception object hashref has expected key';

    is ref $invalid_error, 'HASH', 'exception object error spec is hashref';

    ok exists $invalid_error->{ error }, 'exception object error spec has error key';
    ok exists $invalid_error->{ message }, 'exception object error spec has message key';

    is $invalid_error->{ error }, 'invalid_attribute',
        'expected error type in attribute validation error spec';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
