package Syzygy::Test::Object::Model;

use Test::Class::Moose;
use Test::Most;

use Syzygy::Syntax;
use Syzygy::Object::Model;
use Syzygy::Object::Type;
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

sub test_object_model {
    my $model = Syzygy::Object::Model->new;

    isa_ok $model, 'Syzygy::Object::Model', 'model instantiator retval';

    lives_ok { $model->preload } 'object model preload lives';

    $model->set_object_type('foo', Syzygy::Object::Type->new(
        name => 'foo',
        attributes => {
            foo => Syzygy::Object::Attribute->new(
                name => 'foo',
                value_type => $model->get_value_type('string')
            )
        })
    );

    ok $model->has_object_type('foo'), 'model->has_object_type() for added type';

    lives_ok { $model->new_object('foo') } 'model->new_object(type) lives';

    lives_ok { $model->new_object('foo', { foo => 'abc' }) } 'model->new_object(type, {}) lives';

    my $object = $model->new_object('foo', { foo => 'abc' });

    isa_ok $object, 'Syzygy::Object', 'model->new_object(type, {}) retval';

    isa_ok $object->get_value('foo'), 'Syzygy::Object::Value', 'object->get_value retval';

    is $object->get_value('foo')->value, 'abc', 'object->get_value->value matches';
}

sub test_object_model_read_graph {
    my $model = Syzygy::Object::Model->get_instance;

    my $foo_type = Syzygy::Object::Type->new(
        name => 'foo',
        attributes => {
            foo => Syzygy::Object::Attribute->new(
                name => 'foo',
                value_type => $model->get_value_type('string')
            )
        }
    );

    $model->set_object_type(foo => $foo_type);

    my $ref = $model->read_graph_hash({
        type => 'foo',
        reference => create_uuid_as_string(UUID_V4)
    });

    isa_ok $ref, 'Syzygy::Object::Reference',
        'object model read_graph_hash returned expected reference instance';

    is $ref->type_name, 'foo', 'reference instance has expected type_name';

    my $obj = $model->read_graph_hash({
        type => 'foo',
        reference => create_uuid_as_string(UUID_V4),
        instance => {
            foo => 'abc'
        }
    });

    isa_ok $obj, 'Syzygy::Object',
        'object model read_graph_hash returned expected object instance';

    is $obj->type_name, 'foo', 'object instance has expected type_name';

    ok $obj->has_value('foo'), 'object instance has expected attribute';

    is $obj->get_value('foo')->value, 'abc',
        'object instance has expected attribute value';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
