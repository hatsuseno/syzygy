package Syzygy::Test::Object::ValueTypes::ObjectRef;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::Reference;
use Syzygy::Object::Type;
use Syzygy::Object::ValueTypes::ObjectRef;
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

sub test_object_value_types_object_ref {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::ObjectRef->new }
        'object value type object_ref lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::ObjectRef',
        'object value type object_ref constructor retval';

    is $type->name, 'object_ref', 'type has expected name';

    my $object_type = Syzygy::Object::Type->new(
        name => 'foo'
    );

    my $a = $type->new_value(Syzygy::Object::Reference->new(
        type => $object_type,
        id => create_uuid_as_string(UUID_V4)
    ));

    my $b = $type->new_value(Syzygy::Object::Reference->new(
        type => $object_type,
        id => create_uuid_as_string(UUID_V4)
    ));

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok !$type->equal($a, $b), 'a == b: negative equality';
    ok !$type->not_equal($a, $a), 'a != a: negative non-equality';
    ok $type->not_equal($a, $b), 'a != b: positive non-equality';

    is_deeply $type->deflate_value($a), {
        type => $object_type->name,
        id => $a->value->id,
        instance => undef
    }, 'type value deflation returns object_ref structure';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
