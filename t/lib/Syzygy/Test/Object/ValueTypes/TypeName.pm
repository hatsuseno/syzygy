package Syzygy::Test::Object::ValueTypes::TypeName;

use Test::Class::Moose;

use Syzygy::Object::ValueTypes::TypeName;

sub test_object_value_type_type_name {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::TypeName->new }
        'type_name value type instantiaton lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::TypeName',
        'type_name value type constructor retval';

    is $type->name, 'type_name', 'type_name value type has expected name';

    my $a = $type->new_value('abc');
    my $b = $type->new_value('xyz');

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok !$type->equal($a, $b), 'a == b: negative equality';
    ok $type->not_equal($a, $b), 'a != b: positive inequality';
    ok !$type->not_equal($a, $a), 'a != a: negative inequality';

    my @ok_examples = qw[abc abc_xyz a1_b2_c3 abc/xyz abc123/xyz123 a/b/c];
    my @fail_examples = qw[123 _abc abc/_xyz a1_b2/123 a1_b2/_123];

    lives_ok { $type->new_value($_) } "$_: valid type name" for @ok_examples;

    dies_ok { $type->new_value($_) } "$_: invalid type name" for @fail_examples;

    is_deeply $type->deflate_value($a), 'abc',
        'type value deflation returns plain string';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
