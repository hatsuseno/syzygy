package Syzygy::Test::Object::ValueTypes::Datetime;

use Test::Class::Moose;
use Test::Fatal;

use DateTime;
use Syzygy::Object::ValueTypes::Datetime;
use Syzygy::Object::ValueTypes::String;

sub test_object_value_type_datetime {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Datetime->new }
        'datetime value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Datetime',
        'datetime value type constructor retval';

    is $type->name, 'datetime', 'datetime value type has expected name';

    my $now_dt = DateTime->now;
    my $yesterday_dt = DateTime->now->subtract(days => 1);

    my ($now, $yesterday);

    lives_ok {
        $now = $type->new_value($now_dt)
    } 'datetime value type instantiates now';

    lives_ok {
        $yesterday = $type->new_value($yesterday_dt)
    } 'datetime value type instantiates yesterday';

    isa_ok $now, 'Syzygy::Object::Value', 'datetime value type value instantiator retval';
    isa_ok $yesterday, 'Syzygy::Object::Value', 'datetime value type value instantiator retval';

    ok $type->equal($now, $now), 'now == now: positive equality';
    ok $type->equal($yesterday, $yesterday), 'yesterday == yesterday: positive equality';

    ok !$type->equal($now, $yesterday), 'now == yesterday: negative equality';
    ok !$type->equal($yesterday, $now), 'yesterday == now: negative equality';

    is_deeply $type->deflate_value($now), sprintf('%sZ', $now_dt->iso8601),
        'type value deflation returns Z-postfixed ISO8601 datetime string';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
