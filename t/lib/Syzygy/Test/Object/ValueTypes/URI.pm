package Syzygy::Test::Object::ValueTypes::URI;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::ValueTypes::URI;
use URI;

sub test_object_value_type_uri {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::URI->new }
        'uri value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::URI',
        'uri value type constructor retval';

    is $type->name, 'uri', 'uri value type has expected name';

    my $a = $type->new_value(URI->new('https://example.com'));
    my $b = $type->new_value(URI->new('http://example.com'));

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok $type->not_equal($a, $b), 'a == b: negative inequality';
    ok !$type->equal($a, $b), 'a != b: negative equality';
    ok !$type->not_equal($b, $b), 'b != b: negative inequality';

    # is_deeply to prevent URI's auto-stringification behavior.
    is_deeply $type->deflate_value($a), 'https://example.com',
        'type value deflation returned stringified URI';
}

sub test_object_value_type_uri_coercions {
    my $type = Syzygy::Object::ValueTypes::URI->new;
    
    my $string = Syzygy::Object::ValueTypes::String->new->new_value(
        'http://www.example.com'
    );

    my $uri = $type->coerce($string);

    is $uri->type_name, 'uri', 'coerced value has expected type';
    is $uri->value->as_string, $string->value, 'coerced value preserves content';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
