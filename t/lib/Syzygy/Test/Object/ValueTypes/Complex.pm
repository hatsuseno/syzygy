package Syzygy::Test::Object::ValueTypes::Complex;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::ValueTypes::Complex;

sub test_object_value_type_complex {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Complex->new }
        'complex value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Complex',
        'complex value type constructor retval';

    is $type->name, 'complex', 'complex value type has expected name';

    my $value = $type->new_value({ foo => 'bar' });

    isa_ok $value, 'Syzygy::Object::Value',
        'complex value type value builder retval';

    is ref $value->value, 'HASH', 'complex value instance value is hashref';

    is $value->value->{ foo }, 'bar',
        'complex value instance value has expected key-value pair';

    is_deeply $type->deflate_value($value), { foo => 'bar' },
        'type value deflation returns hashref';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
