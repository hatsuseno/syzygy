package Syzygy::Test::Object::Types::Type;

use Test::Class::Moose;
use Test::Most;
use Test::Fatal;

use Syzygy::Object::Model;
use Syzygy::Object::Types::Type;

sub test_object_type_type_simple {
    my $model = Syzygy::Object::Model->get_instance;

    my $type_hash = {
        type => 'type',
        reference => undef,
        instance => {
            type_name => 'simple_type'
        }
    };

    my $object;
    
    lives_ok { $object = $model->read_graph_hash($type_hash) }
        'model can read simplest object type graph';

    isa_ok $object, 'Syzygy::Object', 'model read_graph_hash retval';
    
    is $object->type->name, 'type', 'object type returns expected name';

    my $type;
    
    lives_ok { $type = $object->type->as_type($object->instance) }
        'type lives when building type instance of self';

    isa_ok $type, 'Syzygy::Object::Type', 'type->as_type retval';

    my @type_attributes = $type->all_attributes;

    ok !$type->ignore_unknown_attributes, 'ignore unknown attributes is false';
    is scalar(@type_attributes), 0, 'set of attributes empty when not provided';

    ok !$model->has_object_type('simple_type'),
        'type inflation does not automatically add type to model';
}

sub test_object_type_type_with_attrs {
    my $model = Syzygy::Object::Model->get_instance;

    my $type_hash = {
        type => 'type',
        reference => undef,
        instance => {
            type_name => 'with_attr',
            attributes => {
                foo => { value_type_name => 'string' }
            }
        }
    };

    my $object;

    lives_ok { $object = $model->read_graph_hash($type_hash) }
        'model can read object type with attrs graph';

    isa_ok $object, 'Syzygy::Object', 'model read_graph_hash retval';

    my $type;

    lives_ok { $type = $object->type->as_type($object->instance) }
        'type lives when building instance of self';

    isa_ok $type, 'Syzygy::Object::Type', 'type->as_type retval';

    $model->set_object_type('with_attr', $type);

    my $with_attr_hash = {
        type => 'with_attr',
        reference => undef,
        instance => {
            foo => 'abc'
        }
    };

    my $with_attr;

    lives_ok { $with_attr = $model->read_graph_hash($with_attr_hash) }
        'model can read with_attr object graph hash';

    isa_ok $with_attr, 'Syzygy::Object', 'model read_graph_hash retval';

    is $with_attr->type_name, 'with_attr',
        'type_name dereference of object has expected type name';
}

sub test_object_type_invalid_attrs {
    my $model = Syzygy::Object::Model->get_instance;

    my $attributes = { foo => 'bar' };
    my $type_hash = {
        type => 'type',
        reference => undef,
        instance => {
            type_name => 'invalid',
            attributes => $attributes
        }
    };

    my $invalid;

    lives_ok { $invalid = $model->read_graph_hash($type_hash) }
        'invalid type hash inflates anyway (attributes value not inspected)';

    my $ex = exception { $invalid->type->as_type($invalid->instance) };

    isa_ok $ex, 'BTTW::Exception::Base', 'type->as_type(invalid) exception';
    is $ex->type, 'params/profile/input', 'expected exception type';

    # Now try creating a type using invalid attribute spec
    $attributes->{ foo } = { foo => 'bar' };

    lives_ok { $invalid = $model->read_graph_hash($type_hash) }
        'invalid type hash inflates anyway (attributes still not inspected)';

    $ex = exception { $invalid->type->as_type($invalid->instance) };

    isa_ok $ex, 'BTTW::Exception::Base', 'type->as_type(invalid) exception';
    is $ex->type, 'params/profile', 'expected exception type';

    ok $ex->object->unknown->{ foo },
        'exception thrown specifies unknown field';

    ok $ex->object->missing('value_type_name'),
        'exception thrown specifies missing value type field';
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the C<LICENSE> file.
