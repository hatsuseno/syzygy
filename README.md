# NAME

Syzygy - Dynamic object interaction library

# DESCRIPTION

Syzygy is a set of libraries used for defining, storing, searching, and
interacting with objects.

Syzygy differentiates itself from, for example, [Moose](https://metacpan.org/pod/Moose) because it works with
fully dynamic object types and instances. At the cost of speed, of course.

The project is still heavily under development, and should be considered beta
quality at best.

# COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the [CONTRIBUTORS](https://metacpan.org/pod/Syzygy::CONTRIBUTORS) file.

Syzygy uses the EUPL license, for more information please have a look at the [LICENSE](https://metacpan.org/pod/Syzygy::LICENSE) file.
